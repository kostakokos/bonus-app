<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240420160203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bonus (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_bonus (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, bonus_id INT DEFAULT NULL, reward VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_3CF5E3CD19EB6921 (client_id), INDEX IDX_3CF5E3CD69545666 (bonus_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_bonus ADD CONSTRAINT FK_3CF5E3CD19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_bonus ADD CONSTRAINT FK_3CF5E3CD69545666 FOREIGN KEY (bonus_id) REFERENCES bonus (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client_bonus DROP FOREIGN KEY FK_3CF5E3CD19EB6921');
        $this->addSql('ALTER TABLE client_bonus DROP FOREIGN KEY FK_3CF5E3CD69545666');
        $this->addSql('DROP TABLE bonus');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE client_bonus');
    }
}
