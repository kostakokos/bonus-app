<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api;

use App\Domain\Repository\BonusRepositoryInterface;
use App\Domain\Repository\ClientRepositoryInterface;
use App\Tests\Tools\DITools;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

use function array_shift;
use function array_pop;
use function current;

class BonusTestCase extends WebTestCase
{
    use DITools;

    private ClientRepositoryInterface $clientRepository;

    private BonusRepositoryInterface $bonusRepository;

    private KernelBrowser $clientApi;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientApi = static::createClient();
        $this->clientRepository = $this->getService(ClientRepositoryInterface::class);
        $this->bonusRepository = $this->getService(BonusRepositoryInterface::class);
    }

    public function test_create_client_bonus(): void
    {
        $clients = $this->clientRepository->findAll();
        $bonuses = $this->bonusRepository->findAll();

        $client = array_pop($clients);
        $bonus = array_shift($bonuses);

        $this->clientApi->request(
            Request::METHOD_POST,
            '/api/bonus/receiving',
            [
                'bonusId' => $bonus->getId(),
                'clientId' => $client->getId(),
                'isBirthday' => true
            ]
        );

        $this->assertResponseIsSuccessful();
        $result = json_decode($this->clientApi->getResponse()->getContent(), true);
        $this->assertEquals($result['reward'], 'hug');
    }

    public function test_client_bonus_list(): void
    {
        $clients = $this->clientRepository->findAll();
        $client = array_pop($clients);

        $this->clientApi->request(
            Request::METHOD_GET,
            '/api/bonus/client/' . $client->getId(),
        );

        $this->assertResponseIsSuccessful();
        $result = json_decode($this->clientApi->getResponse()->getContent(), true);

        $this->assertIsArray($result);

        $item = current($result);
        $this->assertIsArray($item);
        $this->assertArrayHasKey('bonus_id', $item);
        $this->assertArrayHasKey('bonus_name', $item);
        $this->assertArrayHasKey('reward', $item);
        $this->assertArrayHasKey('date_bonus', $item);
    }
}
