<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api;

use App\Domain\Repository\ClientRepositoryInterface;
use App\Tests\Tools\DITools;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

use function current;

class ClientTestCase extends WebTestCase
{
    use DITools;

    private ClientRepositoryInterface $clientRepository;

    private KernelBrowser $clientApi;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientApi = static::createClient();
        $this->clientRepository = $this->getService(ClientRepositoryInterface::class);
    }

    public function test_client_result(): void
    {
        $clients = $this->clientRepository->findAll();
        $client = current($clients);

        $this->clientApi->request(Request::METHOD_GET, '/api/client/' . $client->getId());

        $this->assertResponseIsSuccessful();
        $jsonResult = json_decode($this->clientApi->getResponse()->getContent(), true);
        $this->assertEquals($jsonResult['id'], $client->getId());
    }
}
