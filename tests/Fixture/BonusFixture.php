<?php

declare(strict_types=1);

namespace App\Tests\Fixture;

use App\Application\Bus\CommandBusInterface;
use App\Application\Command\Bonus\BonusCommand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BonusFixture extends Fixture
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 200; $i++) {
            $bonusCommand = new BonusCommand("Bonus number {$i}");
            $this->commandBus->execute($bonusCommand);
        }
    }
}
