<?php

declare(strict_types=1);

namespace App\Tests\Fixture;

use App\Application\Bus\CommandBusInterface;
use App\Application\Command\Client\ClientCommand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ClientFixture extends Fixture
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 40; $i++) {
            $bonusCommand = new ClientCommand($faker->firstName . ' ' . $faker->lastName);
            $this->commandBus->execute($bonusCommand);
        }
    }
}
