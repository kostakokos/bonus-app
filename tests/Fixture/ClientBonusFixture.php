<?php

declare(strict_types=1);

namespace App\Tests\Fixture;

use App\Application\Bus\CommandBusInterface;
use App\Application\Command\ClientBonus\ClientBonusCommand;
use App\Domain\Repository\BonusRepositoryInterface;
use App\Domain\Repository\ClientRepositoryInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ClientBonusFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
        private ClientRepositoryInterface $clientRepository,
        private BonusRepositoryInterface $bonusRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $clients = $this->clientRepository->findAll();
        $bonuses = $this->bonusRepository->findAll();

        for ($i = 0; $i < 30; $i++) {
            $isBirthday = $i % 2 !== 0;

            $clientBonusCommand = new ClientBonusCommand(
                $clients[$i]->getId(),
                $bonuses[$i]->getId(),
                true,
                $isBirthday
            );
            $this->commandBus->execute($clientBonusCommand);
        }
    }

    public function getDependencies(): array
    {
        return [
            ClientFixture::class,
            BonusFixture::class,
        ];
    }
}
