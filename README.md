### Bonus Symfony Application

### Init Project
- docker compose up -d
- docker exec bonus-php composer install
- docker exec bonus-php php bin/console doctrine:migrations:migrate --no-interaction

### Fixtures
- docker exec bonus-php php bin/console doctrine:fixtures:load --no-interaction

### Tests
- docker exec bonus-php php bin/console --env=test doctrine:database:create
- docker exec bonus-php php bin/console --env=test doctrine:migrations:migrate --no-interaction
- docker exec bonus-php php bin/console --env=test doctrine:fixtures:load --no-interaction
- docker exec bonus-php php bin/phpunit tests/Functional/Api/*

### API Swagger http://localhost/api/doc
    
