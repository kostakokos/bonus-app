<?php

declare(strict_types=1);

namespace App\Application\Command\Client;

use App\Application\Command\Interface\CommandInterface;

readonly class ClientCommand implements CommandInterface
{
    public function __construct(
        public string $fullName,
    ) {}
}
