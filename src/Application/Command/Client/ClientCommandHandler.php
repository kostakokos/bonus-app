<?php

declare(strict_types=1);

namespace App\Application\Command\Client;

use App\Application\Command\Interface\CommandHandlerInterface;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepositoryInterface;

class ClientCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ClientRepositoryInterface $bonusRepository,
    ) {}

    public function __invoke(ClientCommand $command): Client
    {
        $bonus = new Client($command->fullName);
        $this->bonusRepository->save($bonus);

        return $bonus;
    }
}
