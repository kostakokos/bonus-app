<?php

declare(strict_types=1);

namespace App\Application\Command\ClientBonus;

use App\Application\Command\Interface\CommandHandlerInterface;
use App\Domain\Entity\ClientBonus;
use App\Domain\Enum\EnumReward;
use App\Domain\Repository\BonusRepositoryInterface;
use App\Domain\Repository\ClientBonusRepositoryInterface;
use App\Domain\Repository\ClientRepositoryInterface;
use Exception;

class ClientBonusCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ClientBonusRepositoryInterface $clientBonusRepository,
        private ClientRepositoryInterface $clientRepository,
        private BonusRepositoryInterface $bonusRepository,
    ) {}

    /**
     * @throws Exception
     */
    public function __invoke(ClientBonusCommand $command): ClientBonus
    {
        $clientBonus = $this->clientBonusRepository->findByClientIdAndBonusId(
            $command->clientId,
            $command->bonusId
        );
        if ($clientBonus) {
            throw new Exception('The client already has a promotion');
        }

        $client = $this->clientRepository->findOneById($command->clientId);
        if (!$client) {
            throw new Exception('Client not found');
        }

        $bonus = $this->bonusRepository->findOneById($command->bonusId);
        if (!$bonus) {
            throw new Exception('Bonus not found');
        }

        $enumReward = match (true) {
            $command->isBirthday => EnumReward::Hug,
            $command->emailVerified => EnumReward::Smile,
            default => throw new Exception("emailVerified or isBirthday field must be true"),
        };

        $clientBonus = new ClientBonus(
            $enumReward,
            $client,
            $bonus
        );
        $this->clientBonusRepository->save($clientBonus);

        return $clientBonus;
    }
}
