<?php

declare(strict_types=1);

namespace App\Application\Command\ClientBonus;

use App\Application\Command\Interface\CommandInterface;

readonly class ClientBonusCommand implements CommandInterface
{
    public function __construct(
        public int $clientId,
        public int $bonusId,
        public ?bool $emailVerified,
        public ?bool $isBirthday,
    ) {}
}
