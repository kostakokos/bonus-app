<?php

declare(strict_types=1);

namespace App\Application\Command\Bonus;

use App\Application\Command\Interface\CommandInterface;

readonly class BonusCommand implements CommandInterface
{
    public function __construct(
        public string $bonusName,
    ) {}
}
