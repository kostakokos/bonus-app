<?php

declare(strict_types=1);

namespace App\Application\Command\Bonus;

use App\Application\Command\Interface\CommandHandlerInterface;
use App\Domain\Entity\Bonus;
use App\Domain\Repository\BonusRepositoryInterface;

class BonusCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private BonusRepositoryInterface $bonusRepository,
    ) {}

    public function __invoke(BonusCommand $command): Bonus
    {
        $bonus = new Bonus($command->bonusName);
        $this->bonusRepository->save($bonus);

        return $bonus;
    }
}
