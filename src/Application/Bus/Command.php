<?php

declare(strict_types=1);

namespace App\Application\Bus;

use App\Application\Command\Interface\CommandInterface;

abstract readonly class Command implements CommandInterface
{
}
