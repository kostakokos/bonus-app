<?php

declare(strict_types=1);

namespace App\Application\Bus;

use App\Application\Query\Interface\QueryInterface;

interface QueryBusInterface
{
    public function execute(QueryInterface $query): mixed;
}
