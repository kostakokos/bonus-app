<?php

declare(strict_types=1);

namespace App\Application\Bus;

use App\Application\Command\Interface\CommandInterface;

interface CommandBusInterface
{
    public function execute(CommandInterface $command): mixed;
}
