<?php

declare(strict_types=1);

namespace App\Application\Query\Client;

use App\Application\Query\Interface\QueryHandlerInterface;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepositoryInterface;

readonly class ClientQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private ClientRepositoryInterface $clientRepository,
    ) {}

    public function __invoke(ClientQuery $query): Client
    {
        return $this->clientRepository->findOneById($query->id);
    }
}
