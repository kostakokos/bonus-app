<?php

declare(strict_types=1);

namespace App\Application\Query\Client;

use App\Application\Query\Interface\QueryInterface;

class ClientQuery implements QueryInterface
{
    public function __construct(
        public int $id
    ) {}
}
