<?php

declare(strict_types=1);

namespace App\Application\Query\ClientBonus;

use App\Application\Query\Interface\QueryHandlerInterface;
use App\Domain\Repository\ClientBonusRepositoryInterface;
use App\Domain\Repository\Pagination\PaginatorInterface;

class ClientBonusPaginationQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private ClientBonusRepositoryInterface $clientBonusRepository,
    ) {}

    public function __invoke(ClientBonusPaginationQuery $query): PaginatorInterface
    {
        return $this->clientBonusRepository->findAllClientBonusPagination(
            $query->clientId,
            $query->currentPage,
            $query->maxPerPage
        );
    }
}
