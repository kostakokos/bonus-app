<?php

declare(strict_types=1);

namespace App\Application\Query\ClientBonus;

use App\Application\Query\Interface\QueryInterface;

readonly class ClientBonusPaginationQuery implements QueryInterface
{
    public function __construct(
        public int $clientId,
        public int $currentPage = 1,
        public int $maxPerPage = 20,
    ) {}
}
