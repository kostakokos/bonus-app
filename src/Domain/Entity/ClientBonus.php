<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Enum\EnumReward;
use DateTime;

class ClientBonus
{
    private int $id;

    private DateTime $createdAt;

    public function __construct(
        private readonly EnumReward $reward,
        private readonly Client $client,
        private readonly Bonus $bonus,
    ) {
        $this->createdAt = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getBonus(): Bonus
    {
        return $this->bonus;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getReward(): EnumReward
    {
        return $this->reward;
    }
}
