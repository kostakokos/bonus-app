<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class Client
{
    private int $id;

    public function __construct(
        private string $fullName,
    ) {}

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }
}
