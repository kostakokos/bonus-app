<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class Bonus
{
    private int $id;

    public function __construct(
        private string $name,
    ) {}

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
