<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum EnumReward: string
{
    case Smile = 'smile';

    case Hug = 'hug';
}
