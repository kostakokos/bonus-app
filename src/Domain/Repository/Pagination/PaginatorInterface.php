<?php

declare(strict_types=1);

namespace App\Domain\Repository\Pagination;

use IteratorAggregate;

interface PaginatorInterface extends IteratorAggregate
{
}
