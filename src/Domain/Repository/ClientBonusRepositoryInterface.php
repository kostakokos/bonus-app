<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\ClientBonus;
use App\Domain\Repository\Pagination\PaginatorInterface;

interface ClientBonusRepositoryInterface
{
    public function save(ClientBonus $entity): void;

    public function findByClientIdAndBonusId(int $clientId, int $bonusId): ?ClientBonus;

    public function findAllClientBonusPagination(
        int $clientId,
        int $currentPage,
        int $maxPerPage,
    ): PaginatorInterface;
}
