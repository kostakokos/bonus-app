<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Bonus;

interface BonusRepositoryInterface
{
    public function findOneById(int $id): ?Bonus;

    public function save(Bonus $entity): void;
}
