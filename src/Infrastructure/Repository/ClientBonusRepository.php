<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\ClientBonus;
use App\Domain\Repository\ClientBonusRepositoryInterface;
use App\Domain\Repository\Pagination\PaginatorInterface;
use App\Infrastructure\Repository\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ClientBonusRepository extends ServiceEntityRepository implements ClientBonusRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientBonus::class);
    }

    public function save(ClientBonus $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function findByClientIdAndBonusId(int $clientId, int $bonusId): ?ClientBonus
    {
        return $this->findOneBy([
            'client' => $clientId,
            'bonus' => $bonusId,
        ]);
    }

    public function findAllClientBonusPagination(
        int $clientId,
        int $currentPage,
        int $maxPerPage
    ): PaginatorInterface {
        $qb = $this->createQueryBuilder('cb');
        $qb->addOrderBy('cb.id', 'DESC');

        return new Paginator($qb, $currentPage, $maxPerPage);
    }
}
