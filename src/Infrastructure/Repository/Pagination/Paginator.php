<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Pagination;

use App\Domain\Repository\Pagination\PaginatorInterface;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Traversable;

class Paginator implements PaginatorInterface
{
    private Pagerfanta $paginator;

    public function __construct(
        private QueryBuilder $queryBuilder,
        private int $currentPage = 1,
        private int $maxPerPage = 20,
    ) {
        $this->initPaginator();
    }

    private function initPaginator(): void
    {
        $queryAdapter = new QueryAdapter($this->queryBuilder);
        $this->paginator = new Pagerfanta($queryAdapter);
        $this->paginator->setMaxPerPage($this->maxPerPage);
        $this->paginator->setCurrentPage($this->currentPage);
    }

    public function getIterator(): Traversable
    {
        return $this->paginator->getIterator();
    }
}
