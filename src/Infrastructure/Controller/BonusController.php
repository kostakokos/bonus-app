<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Application\Bus\CommandBusInterface;
use App\Application\Bus\QueryBusInterface;
use App\Application\Command\ClientBonus\ClientBonusCommand;
use App\Application\Query\ClientBonus\ClientBonusPaginationQuery;
use App\Domain\Entity\Client;
use App\Domain\Entity\ClientBonus;
use App\Domain\Repository\Pagination\PaginatorInterface;
use App\Infrastructure\Controller\Input\BonusReceivingDTO;
use App\Infrastructure\Controller\Output\ClientBonusResponse;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Attributes as OAT;

#[AsController]
#[Route('/api/bonus')]
readonly class BonusController
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface   $queryBus,
        private ValidatorInterface  $validator
    ) {}

    #[Route('/receiving', methods: ['POST'])]
    #[OAT\Post(
        path: '/api/bonus/receiving',
        description: 'Endpoint for receiving a bonus.',
        summary: 'Receiving a bonus',
        requestBody: new OAT\RequestBody(
            required: true,
            content: new OAT\MediaType(
                mediaType: 'application/x-www-form-urlencoded',
                schema: new OAT\Schema(
                    required: ['bonusId', 'clientId'],
                    properties: [
                        new OAT\Property(property: 'bonusId', description: 'ID of the bonus', type: 'integer'),
                        new OAT\Property(property: 'clientId', description: 'ID of the client', type: 'integer'),
                        new OAT\Property(property: 'emailVerified', description: 'Whether email is verified', type: 'boolean'),
                        new OAT\Property(property: 'isBirthday', description: 'Whether it is the client\'s birthday', type: 'boolean'),
                    ],
                    type: 'object'
                )
            )
        ),
        responses: [
            new OAT\Response(
                response: '200',
                description: 'Bonus successfully received',
                content: new OAT\MediaType(
                    mediaType: 'application/json',
                    schema: new OAT\Schema(
                        properties: [
                            new OAT\Property(property: 'id', description: 'ID of the received bonus', type: 'integer'),
                            new OAT\Property(property: 'clientId', description: 'ID of the client', type: 'integer'),
                            new OAT\Property(property: 'bonus', description: 'Name of the bonus', type: 'string'),
                            new OAT\Property(property: 'reward', description: 'Type of reward', type: 'string'),
                        ],
                        type: 'object'
                    )
                )
            )
        ]
    )]
    public function receivingBonus(Request $request): JsonResponse
    {
        $dtoRequest = BonusReceivingDTO::fillFromRequest($request);
        $errors = $this->validator->validate($dtoRequest);

        if (count($errors) > 0) {
            throw new BadRequestHttpException((string)$errors);
        }

        $commandClientBonus = new ClientBonusCommand(
            $dtoRequest->clientId,
            $dtoRequest->bonusId,
            $dtoRequest->emailVerified,
            $dtoRequest->isBirthday
        );

        try {
            /** @var ClientBonus $clientBonus */
            $clientBonus = $this->commandBus->execute($commandClientBonus);
        } catch (Exception $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        return new JsonResponse([
            'id' => $clientBonus->getId(),
            'clientId' => $clientBonus->getClient()->getId(),
            'bonus' => $clientBonus->getBonus()->getName(),
            'reward' => $clientBonus->getReward()->value
        ]);
    }

    #[Route('/client/{id<\d+>}', methods: ['GET'])]
    #[OAT\Get(
        path: '/api/bonus/client/{id}',
        description: 'Retrieve a paginated list of bonuses for a specified client.',
        summary: 'Get bonuses for a client',
        parameters: [
            new OAT\Parameter(
                name: 'id',
                description: 'ID of the client',
                in: 'path',
                required: true,
                schema: new OAT\Schema(type: 'integer')
            ),
            new OAT\Parameter(
                name: 'page',
                description: 'Page number',
                in: 'query',
                required: false,
                schema: new OAT\Schema(type: 'integer')
            ),
            new OAT\Parameter(
                name: 'maxPerPage',
                description: 'Maximum number of results per page',
                in: 'query',
                required: false,
                schema: new OAT\Schema(type: 'integer')
            ),
        ],
        responses: [
            new OAT\Response(
                response: '200',
                description: 'A list of bonuses for the client',
                content: new OAT\MediaType(
                    mediaType: 'application/json',
                    schema: new OAT\Schema(
                        type: 'array',
                        items: new OAT\Items(
                            properties: [
                                new OAT\Property(property: 'bonus_id', description: 'ID of the bonus', type: 'integer'),
                                new OAT\Property(property: 'bonus_name', description: 'Name of the bonus', type: 'string'),
                                new OAT\Property(property: 'reward', description: 'Type of reward', type: 'string'),
                                new OAT\Property(property: 'date_bonus', description: 'Date and time of the bonus', type: 'string', format: 'date-time'),
                            ],
                            type: 'object'
                        )
                    )
                )
            )
        ]
    )]
    public function allBonusClient(Client $client, Request $request): ClientBonusResponse
    {
        $page = (int) $request->get('page', 1);
        $maxPerPage = (int) $request->get('maxPerPage', 20);

        $clientBonusPaginationQuery = new ClientBonusPaginationQuery(
            $client->getId(),
            $page,
            $maxPerPage,
        );

        try {
            /** @var PaginatorInterface $clientBonuses */
            $clientBonuses = $this->queryBus->execute($clientBonusPaginationQuery);
        } catch (Exception $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        return new ClientBonusResponse($clientBonuses);
    }
}
