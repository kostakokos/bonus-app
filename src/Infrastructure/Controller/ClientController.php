<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use OpenApi\Attributes as OAT;
use App\Domain\Entity\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
#[Route('/api/client')]
class ClientController
{
    #[Route('/{id<\d+>}', methods: ['GET'])]
    #[OAT\Get(
        path: '/api/client/{id}',
        description: 'Retrieve the details of a client based on their ID.',
        summary: 'Get client details by ID',
        parameters: [
            new OAT\Parameter(
                name: 'id',
                description: 'ID of the client to retrieve details for.',
                in: 'path',
                required: true,
                schema: new OAT\Schema(
                    description: 'The ID of the client',
                    type: 'string'
                )
            )
        ],
        responses: [
            new OAT\Response(
                response: '200',
                description: 'Client details retrieved successfully',
                content: new OAT\MediaType(
                    mediaType: 'application/json',
                    schema: new OAT\Schema(
                        properties: [
                            new OAT\Property(property: 'id', description: 'ID of the client', type: 'string'),
                            new OAT\Property(property: 'full_name', description: 'Full name of the client', type: 'string')
                        ],
                        type: 'object'
                    )
                )
            ),
            new OAT\Response(
                response: '404',
                description: 'Client not found',
                content: new OAT\MediaType(
                    mediaType: 'application/json',
                    schema: new OAT\Schema(
                        properties: [
                            new OAT\Property(property: 'message', description: 'Error message indicating that the client was not found', type: 'string')
                        ],
                        type: 'object'
                    )
                )
            )
        ]
    )]
    public function client(Client $client): JsonResponse
    {
        return new JsonResponse([
            'id' => $client->getId(),
            'full_name' => $client->getFullname()
        ]);
    }
}
