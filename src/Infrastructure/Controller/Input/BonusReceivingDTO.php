<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Input;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BonusReceivingDTO
{
    #[Assert\Type(type:'integer')]
    #[Assert\NotBlank]
    public ?int $clientId;

    #[Assert\Type(type:'integer')]
    #[Assert\NotBlank]
    public ?int $bonusId;

    #[Assert\Expression(
        "this.emailVerified === true || this.isBirthday === true",
        message: 'emailVerified or isBirthday field must be true',
    )]
    public ?bool $emailVerified;

    public ?bool $isBirthday;

    public static function fillFromRequest(Request $request): BonusReceivingDTO
    {
        $result = new self();
        $result->clientId = (int) $request->request->get('clientId');
        $result->bonusId = (int) $request->request->get('bonusId');
        $result->emailVerified = (bool) $request->request->get('emailVerified');
        $result->isBirthday = (bool) $request->request->get('isBirthday');

        return $result;
    }
}
