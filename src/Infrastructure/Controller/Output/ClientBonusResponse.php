<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Output;

use App\Domain\Entity\ClientBonus;
use App\Domain\Repository\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientBonusResponse extends JsonResponse
{
    public function __construct(PaginatorInterface $clientBonuses)
    {
        parent::__construct($this->mapParams($clientBonuses));
    }

    private function mapParams(PaginatorInterface $clientBonuses): array
    {
        $result = [];
        /** @var ClientBonus $clientBonus */
        foreach ($clientBonuses as $clientBonus) {
            $result[] = [
                'bonus_id' => $clientBonus->getBonus()->getId(),
                'bonus_name' => $clientBonus->getBonus()->getName(),
                'reward' => $clientBonus->getReward()->value,
                'date_bonus' => $clientBonus->getCreatedAt()->format('Y-m-d:H:m'),
            ];
        }

        return $result;
    }
}
